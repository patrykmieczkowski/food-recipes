## Food Recipes

Zostały zrealizowane wszystkie założenia dostarczone w dokumencie
1. Aplikacja pobiera pierwsze 50 przepisów z godt.no
2. Możliwe jest dynamiczne wyszukanie po nazwie przepisu oraz składnikach
3. Aplikacja działa offline po pierwszym poprawnym pobraniu danych
4. Layouty zostały przygotowane tak aby w czytelny sposób przedstawić przepisy :)