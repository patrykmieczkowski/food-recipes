package com.mieczkowskidev.foodrecipes.adapter

import android.content.Context
import android.os.Build
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.mieczkowskidev.foodrecipes.R
import com.mieczkowskidev.foodrecipes.inflate
import com.mieczkowskidev.foodrecipes.model.Ingredient
import com.mieczkowskidev.foodrecipes.model.Recipe
import kotlinx.android.synthetic.main.adapter_recipe_item.view.*
import javax.inject.Inject

/**
 * Created by Patryk Mieczkowski on 22.03.2018
 */
class RecipesAdapter @Inject constructor(val context: Context) : RecyclerView.Adapter<RecipesAdapter.RecipesViewHolder>() {

    private var recipesList: List<Recipe> = ArrayList()

    companion object {
        private val TAG = RecipesAdapter::class.java.simpleName
    }

    fun refreshRecipesListData(recipesList: List<Recipe>) {
        this.recipesList = recipesList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipesViewHolder {
        val iv = parent.inflate(R.layout.adapter_recipe_item)
        return RecipesViewHolder(iv)
    }

    override fun getItemCount(): Int {
        return recipesList.size
    }

    override fun onBindViewHolder(holder: RecipesViewHolder, position: Int) {
        val recipe = recipesList[position]
        holder.bindViews(recipe, context)
    }

    class RecipesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var view: View = itemView
        var recipe: Recipe? = null

        fun bindViews(recipe: Recipe, context: Context) {
            this.recipe = recipe

            // TITLE
            view.recipe_item_title.text = recipe.title

            // DESCRIPTION
            val description = prepareDescriptionText(recipe.description)
            view.recipe_item_description.text = description

            // INGREDIENTS
            val noIngredientsFound = context.getString(R.string.no_ingredients_found)
            val ingredients = prepareIngredientsList(recipe.ingredients, noIngredientsFound)
            val ingredientsFull = context.getString(R.string.ingredients) + " " + ingredients
            view.recipe_item_ingredients.text = ingredientsFull

            // IMAGE
            Glide.with(context)
                    .load(recipe.images[0].url)
                    .into(view.recipe_item_image)
        }

        private fun prepareDescriptionText(description: String): String {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(description, Html.FROM_HTML_MODE_LEGACY).toString()
            } else {
                Html.fromHtml(description).toString()
            }
        }

        private fun prepareIngredientsList(ingredients: List<Ingredient>, noIngredientsFound: String): String {
            return if (ingredients.isNotEmpty()) {
                val sb = StringBuilder()
                for (ingrid in ingredients) {
                    if (!ingrid.name.equals("")) {
                        sb.append(ingrid.name)
                        sb.append(", ")
                    }
                }
                return if (sb.toString().isNotEmpty())
                    sb.toString()
                else
                    noIngredientsFound
            } else {
                noIngredientsFound
            }
        }
    }

}