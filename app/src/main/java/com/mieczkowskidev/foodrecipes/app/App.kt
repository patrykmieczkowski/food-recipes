package com.mieczkowskidev.foodrecipes.app

import android.app.Application
import io.realm.Realm

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
class App : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        initDagger()
        initRealm()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    private fun initRealm() {
        Realm.init(this)
    }

}