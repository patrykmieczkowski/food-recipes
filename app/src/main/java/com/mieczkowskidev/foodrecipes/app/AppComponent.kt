package com.mieczkowskidev.foodrecipes.app

import android.content.Context
import com.bumptech.glide.Glide
import com.mieczkowskidev.foodrecipes.database.DatabaseModule
import com.mieczkowskidev.foodrecipes.image.ImageModule
import com.mieczkowskidev.foodrecipes.network.NetworkModule
import com.mieczkowskidev.foodrecipes.network.NetworkService
import dagger.Component
import io.realm.RealmConfiguration

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
@AppScope
@Component(modules = [(AppModule::class), (NetworkModule::class), (ImageModule::class), (DatabaseModule::class)])
interface AppComponent {

    fun getAppContext(): Context

    fun getGlide(): Glide

    fun getNetworkService(): NetworkService

    fun getRealmConfiguration(): RealmConfiguration

}