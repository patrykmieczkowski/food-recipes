package com.mieczkowskidev.foodrecipes.app

import javax.inject.Scope


/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
@Scope
@Retention
annotation class AppScope