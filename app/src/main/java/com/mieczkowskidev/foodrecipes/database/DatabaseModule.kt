package com.mieczkowskidev.foodrecipes.database

import android.content.Context
import com.mieczkowskidev.foodrecipes.app.AppModule
import com.mieczkowskidev.foodrecipes.app.AppScope
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
@Module(includes = [(AppModule::class)])
class DatabaseModule {

    @Provides
    @AppScope
    fun provideRealm(context: Context, realmConfiguration: RealmConfiguration): Realm {
        Realm.init(context)
        return Realm.getInstance(realmConfiguration)
    }

    @Provides
    @AppScope
    fun provideRealmConfiguration(): RealmConfiguration =
            RealmConfiguration.Builder()
                    .schemaVersion(0)
                    .build()
}