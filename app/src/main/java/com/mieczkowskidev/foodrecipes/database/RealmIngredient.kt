package com.mieczkowskidev.foodrecipes.database

import io.realm.RealmList
import io.realm.RealmObject
import java.util.*

/**
 * Created by Patryk Mieczkowski on 22.03.2018
 */
open class RealmIngredient constructor(var name : String = "") : RealmObject() {

    var id: String = UUID.randomUUID().toString()

}