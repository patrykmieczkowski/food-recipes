package com.mieczkowskidev.foodrecipes.database

import io.realm.RealmList
import io.realm.RealmObject
import java.util.*

/**
 * Created by Patryk Mieczkowski on 22.03.2018
 */
open class RealmRecipe constructor(var title: String = "", var description: String = "",
                                   var images: RealmImageDetails? = null, var ingredients: RealmList<RealmIngredient>? = null) : RealmObject() {

    var id: String = UUID.randomUUID().toString()

}