package com.mieczkowskidev.foodrecipes.image

import android.content.Context
import com.bumptech.glide.Glide
import com.mieczkowskidev.foodrecipes.app.AppModule
import com.mieczkowskidev.foodrecipes.app.AppScope
import dagger.Module
import dagger.Provides

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
@Module(includes = [(AppModule::class)])
class ImageModule {

    @Provides
    @AppScope
    fun provideGlideInstance(context: Context): Glide = Glide.get(context)
}