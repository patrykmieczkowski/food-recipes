package com.mieczkowskidev.foodrecipes.main

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.jakewharton.rxbinding2.widget.textChanges
import com.mieczkowskidev.foodrecipes.R
import com.mieczkowskidev.foodrecipes.adapter.RecipesAdapter
import com.mieczkowskidev.foodrecipes.app.App
import com.mieczkowskidev.foodrecipes.main.di.DaggerMainActivityComponent
import com.mieczkowskidev.foodrecipes.main.di.MainActivityModule
import com.mieczkowskidev.foodrecipes.model.Recipe
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainPresenter

    @Inject
    lateinit var adapter: RecipesAdapter

    private lateinit var linearLayoutManager: LinearLayoutManager

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initInjection()
        initPresenter()
        initRecycler()
        initSearch()
    }

    private fun initInjection() {
        DaggerMainActivityComponent.builder()
                .mainActivityModule(MainActivityModule(this))
                .appComponent((application as App).appComponent)
                .build()
                .injectMainActivity(this)
    }

    private fun initPresenter() {
        presenter.attachView(this)
    }

    private fun initRecycler() {
        linearLayoutManager = LinearLayoutManager(this)
        recipes_list_recycler.layoutManager = linearLayoutManager
        recipes_list_recycler.adapter = adapter
    }

    private fun initSearch() {
        recipes_search.setHintTextColor(ContextCompat.getColor(this, R.color.foxGrey))

        recipes_search
                .textChanges()
                .skipInitialValue()
                .debounce(400, TimeUnit.MILLISECONDS)
                .subscribe(
                        { presenter.filterList(it.toString()) },
                        { Log.e(TAG, "error - $it") }
                )
    }


    override fun refreshRecipesListData(recipesList: List<Recipe>) {
        adapter.refreshRecipesListData(recipesList)
        adapter.notifyDataSetChanged()
    }

    override fun showProgress() {
        manageDownloadViews(View.VISIBLE, View.GONE, View.GONE)
    }

    override fun hideProgress() {
        manageDownloadViews(View.GONE, View.VISIBLE, View.GONE)
    }

    override fun showNoContent() {
        manageDownloadViews(View.GONE, View.GONE, View.VISIBLE)
    }

    private fun manageDownloadViews(progressBar: Int, recyclerView: Int, noContent: Int) {
        recipes_no_content.visibility = progressBar
        recipes_list_recycler.visibility = recyclerView
        recipes_no_content.visibility = noContent
    }

}
