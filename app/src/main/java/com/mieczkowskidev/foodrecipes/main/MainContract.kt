package com.mieczkowskidev.foodrecipes.main

import com.mieczkowskidev.foodrecipes.model.Recipe
import io.reactivex.Single

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
interface MainContract {

    interface View {
        fun refreshRecipesListData(recipesList: List<Recipe>)
        fun showProgress()
        fun hideProgress()
        fun showNoContent()
    }

    interface Presenter {
        fun attachView(view: View)
        fun detachView()
        fun filterList(query: String)
    }

    interface Provider {

        fun getRecipesFromAPI(tags: String = "", size: String = "thumbnail-medium",
                              ratio: Int = 1, limit: Int = 50, from: Int = 0): Single<List<Recipe>>

        fun getRecipesFromDatabase(): Single<List<Recipe>>
        fun saveRecipesToDatabase(recipesList: List<Recipe>): Single<List<Recipe>>
        fun areThereRecipesInDatabase(): Boolean
        fun getRecipesFromDatabaseWithQuery(query: String): Single<List<Recipe>>
    }
}