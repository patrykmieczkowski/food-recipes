package com.mieczkowskidev.foodrecipes.main

import android.util.Log
import com.mieczkowskidev.foodrecipes.model.Recipe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
class MainPresenter @Inject constructor() : MainContract.Presenter {

    companion object {
        private val TAG = MainPresenter::class.java.simpleName
    }

    var view: MainContract.View? = null

    @Inject
    lateinit var provider: MainContract.Provider

    override fun attachView(view: MainContract.View) {
        this.view = view

        getRecipes()
    }

    private fun getRecipes() {
        view?.showProgress()

        if (provider.areThereRecipesInDatabase()) {
            Log.d(TAG, "database flow")
            getRecipesDatabaseFlow()
        } else {
            Log.d(TAG, "network flow")
            getRecipesNetworkFlow()
        }
    }

    private fun getRecipesNetworkFlow() {
        provider.getRecipesFromAPI()
                .flatMap { provider.saveRecipesToDatabase(it) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { getRecipesHandleSuccess(it) },
                        { getRecipesHandleError(it) }
                )
    }

    private fun getRecipesDatabaseFlow() {
        provider.getRecipesFromDatabase()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { getRecipesHandleSuccess(it) },
                        { getRecipesHandleError(it) }
                )
    }

    override fun filterList(query: String) {
        Log.d(TAG, "filterList with query - $query")
        provider.getRecipesFromDatabaseWithQuery(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { getRecipesHandleSuccess(it) },
                        { getRecipesHandleError(it) }
                )
    }

    private fun getRecipesHandleError(throwable: Throwable) {
        Log.e(TAG, "getRecipes error - $throwable")
        view?.showNoContent()
    }

    private fun getRecipesHandleSuccess(recipesList: List<Recipe>) {
        view?.hideProgress()
        view?.refreshRecipesListData(recipesList)
    }

    override fun detachView() {
        this.view = null
    }

}