package com.mieczkowskidev.foodrecipes.main

import android.content.Context
import com.mieczkowskidev.foodrecipes.database.RealmImageDetails
import com.mieczkowskidev.foodrecipes.database.RealmIngredient
import com.mieczkowskidev.foodrecipes.database.RealmRecipe
import com.mieczkowskidev.foodrecipes.model.ImageDetails
import com.mieczkowskidev.foodrecipes.model.Ingredient
import com.mieczkowskidev.foodrecipes.model.Recipe
import com.mieczkowskidev.foodrecipes.network.NetworkService
import io.reactivex.Single
import io.realm.Case
import io.realm.Realm
import io.realm.RealmList
import javax.inject.Inject

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
class RecipeProvider @Inject constructor(var context: Context, var networkService: NetworkService) : MainContract.Provider {

    override fun getRecipesFromAPI(tags: String, size: String,
                                   ratio: Int, limit: Int, from: Int): Single<List<Recipe>> =
            networkService.getRecipes(tags, size, ratio, limit, from)

    override fun areThereRecipesInDatabase(): Boolean {
        val realm = Realm.getDefaultInstance()
        val size = realm.where(RealmRecipe::class.java).findAll().size
        realm.close()
        return (size > 0)
    }

    override fun saveRecipesToDatabase(recipesList: List<Recipe>): Single<List<Recipe>> {
        val realm = Realm.getDefaultInstance()

        realm.beginTransaction()
        realm.deleteAll()

        var recipesRealmList = RealmList<RealmRecipe>()
        for (recipe in recipesList) {
            var realmRecipe = RealmRecipe()
            realmRecipe.title = recipe.title
            realmRecipe.description = recipe.description
            realmRecipe.images = RealmImageDetails(recipe.images[0].url)

            var ingredientList = RealmList<RealmIngredient>()
            for (ingred in recipe.ingredients) {
                ingredientList.add(RealmIngredient(ingred.name))
            }
            realmRecipe.ingredients = ingredientList
            recipesRealmList.add(realmRecipe)
        }
        realm.copyToRealm(recipesRealmList)
        realm.commitTransaction()
        realm.close()

        return Single.just(recipesList)
    }

    override fun getRecipesFromDatabase(): Single<List<Recipe>> {
        val realm = Realm.getDefaultInstance()
        val data = realm.where(RealmRecipe::class.java).findAll()

        var recipeList = ArrayList<Recipe>()
        for (realmRecipe in data) {
            recipeList.add(convertRealmObjectToPojo(realmRecipe))
        }
        realm.close()
        return Single.just(recipeList)
    }

    override fun getRecipesFromDatabaseWithQuery(query: String): Single<List<Recipe>> {
        val realm = Realm.getDefaultInstance()

        val data = realm.where(RealmRecipe::class.java)
                .beginGroup()
                .contains("title", query, Case.INSENSITIVE)
                .or()
                .contains("ingredients.name", query, Case.INSENSITIVE)
                .endGroup()
                .findAll()

        var recipeList = ArrayList<Recipe>()
        for (realmRecipe in data) {
            recipeList.add(convertRealmObjectToPojo(realmRecipe))
        }

        realm.close()
        return Single.just(recipeList)
    }

    fun convertRealmObjectToPojo(realmRecipe: RealmRecipe): Recipe {
        var recipe = Recipe()
        recipe.title = realmRecipe.title
        recipe.description = realmRecipe.description
        var imageDetails = ImageDetails(realmRecipe.images!!.url)
        var imageList = ArrayList<ImageDetails>()
        imageList.add(imageDetails)
        recipe.images = imageList

        var ingredientsList = ArrayList<Ingredient>()
        for (ingred in realmRecipe.ingredients!!) {
            ingredientsList.add(Ingredient(ingred.name))
        }
        recipe.ingredients = ingredientsList

        return recipe
    }
}