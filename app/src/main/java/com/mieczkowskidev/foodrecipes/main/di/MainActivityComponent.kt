package com.mieczkowskidev.foodrecipes.main.di

import com.mieczkowskidev.foodrecipes.app.AppComponent
import com.mieczkowskidev.foodrecipes.main.MainActivity
import dagger.Component

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
@MainActivityScope
@Component(modules = [(MainActivityModule::class)], dependencies = [(AppComponent::class)])
interface MainActivityComponent {

    fun injectMainActivity(mainActivity: MainActivity)
}