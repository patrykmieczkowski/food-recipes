package com.mieczkowskidev.foodrecipes.main.di

import com.mieczkowskidev.foodrecipes.main.MainActivity
import com.mieczkowskidev.foodrecipes.main.MainContract
import com.mieczkowskidev.foodrecipes.main.MainPresenter
import com.mieczkowskidev.foodrecipes.main.RecipeProvider
import dagger.Module
import dagger.Provides

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
@Module
class MainActivityModule(private val mainActivity: MainActivity) {

    @Provides
    @MainActivityScope
    fun provideMainActivity(): MainActivity = mainActivity

    @Provides
    @MainActivityScope
    fun provideMainPresenter(mainPresenter: MainPresenter): MainContract.Presenter = mainPresenter

    @Provides
    @MainActivityScope
    fun provideRecipeProvider(recipeProvider: RecipeProvider): MainContract.Provider = recipeProvider
}