package com.mieczkowskidev.foodrecipes.main.di

import javax.inject.Scope

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
@Scope
@Retention
annotation class MainActivityScope