package com.mieczkowskidev.foodrecipes.model

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
data class Recipe(var title: String = "", var description: String = "",
                  var images: List<ImageDetails> = ArrayList(), var ingredients: List<Ingredient> = ArrayList())

data class Ingredient(var name: String = "")

data class ImageDetails(var url: String = "")
