package com.mieczkowskidev.foodrecipes.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mieczkowskidev.foodrecipes.app.AppScope
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
@Module
class NetworkModule {

    companion object {
        const val BASE_URL = "http://www.godt.no/"
    }

    @Provides
    @AppScope
    fun provideNetworkService(retrofit: Retrofit): NetworkService = retrofit.create(NetworkService::class.java)

    @Provides
    @AppScope
    fun provideRetrofit(baseUrl: String, gson: Gson, okHttpClient: OkHttpClient):
            Retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

    @Provides
    @AppScope
    fun provideBaseUrl(): String = BASE_URL

    @Provides
    @AppScope
    fun provideGson(): Gson = GsonBuilder()
            .create()

    @Provides
    @AppScope
    fun provideOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

    @Provides
    @AppScope
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return logging
    }
}