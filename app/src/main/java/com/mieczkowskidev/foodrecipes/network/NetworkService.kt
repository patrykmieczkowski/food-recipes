package com.mieczkowskidev.foodrecipes.network

import com.mieczkowskidev.foodrecipes.model.Recipe
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Patryk Mieczkowski on 21.03.2018
 */
interface NetworkService {

    @GET("api/getRecipesListDetailed")
    fun getRecipes(@Query("tags") tags: String,
                   @Query("size") size: String,
                   @Query("ratio") ratio: Int,
                   @Query("limit") limit: Int,
                   @Query("from") from: Int): Single<List<Recipe>>
}